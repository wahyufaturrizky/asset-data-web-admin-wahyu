import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.css';
import 'antd/dist/antd.css';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import { ProtectedRoute } from './protectedRoute';
import ProfileSetting from './pages/ProfileSetting';
import Supplier from './pages/Supplier';

export const App = () => {
	return (
		<Router>
			<Switch>
				<Route exact path='/' component={Login} />
				<ProtectedRoute exact path='/dashboard' component={Dashboard} />
				<ProtectedRoute
					exact
					path='/profile-setting'
					component={ProfileSetting}
				/>
				<ProtectedRoute exact path='/supplier' component={Supplier} />
				<Route path='*' component={() => '404 NOT FOUND'} />
			</Switch>
		</Router>
	);
};

export default App;
