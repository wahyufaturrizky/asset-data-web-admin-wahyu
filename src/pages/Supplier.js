import {
	LoadingOutlined,
	EyeOutlined,
	FileAddOutlined,
} from '@ant-design/icons';
import {
	Layout,
	Modal,
	Space,
	Spin,
	Table,
	Button,
	Form,
	Input,
	Row,
	Col,
	Select,
} from 'antd';
import React, { useEffect, useState } from 'react';
import {
	deleteSupplier,
	getListCountry,
	getListSupplier,
	postNewSupplier,
	updateSupplier,
} from '../services/retrieveData';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
const { Content } = Layout;
const { Option } = Select;

const formItemLayout = {
	labelCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 8,
		},
	},
	wrapperCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 16,
		},
	},
};

const Supplier = (props) => {
	const prefixSelector = (
		<Form.Item name='prefix' noStyle>
			<Select
				style={{
					width: 70,
				}}>
				<Option value='62'>+62</Option>
				<Option value='87'>+87</Option>
			</Select>
		</Form.Item>
	);

	const [modal, contextHolder] = Modal.useModal();
	const [form] = Form.useForm();
	const [listSupplier, setListSupplier] = useState([]);
	const [listCountry, setListCountry] = useState([]);
	const [isModalActionVisible, setIsModalActionVisible] = useState({
		dataRow: null,
		typeAction: '',
		isShowModalAction: false,
	});
	const [isModalVisible, setIsModalVisible] = useState({
		isShowModal: false,
		message: '',
	});
	const [isLoading, setIsLoading] = useState(false);
	const { isShowModal, message } = isModalVisible;
	const { isShowModalAction, typeAction, dataRow } = isModalActionVisible;

	useEffect(() => {
		const fetchListSupplier = async () => {
			setIsModalVisible({
				message: 'Harap bersabar halaman sedang kami muat',
				isShowModal: true,
			});
			const res = await getListSupplier().catch(() =>
				setIsModalVisible({ ...isModalVisible, isShowModal: false })
			);

			if (res && res.status === 200) {
				let tempData = [];
				res.data.data.forEach((data, index) => {
					const {
						name,
						contact_person,
						contact_number,
						city,
						comment,
						contact_number_alternate,
						country_code,
						created_at,
						email,
						guid,
						postcode,
						registration_number,
						state,
						street,
						tax_number,
						updated_at,
						website,
					} = data;
					tempData.push({
						key: index + 1,
						name,
						contact_person: contact_person || 'N/A',
						contact_number: contact_number || 'N/A',
						city,
						comment,
						contact_number_alternate,
						country_code,
						created_at,
						email,
						guid,
						postcode,
						registration_number,
						state,
						street,
						tax_number,
						updated_at,
						website,
					});
				});
				setListSupplier(tempData);
				setIsModalVisible({ ...isModalVisible, isShowModal: false });
			}
		};

		const fetchListCountry = async () => {
			const res = await getListCountry().catch(() =>
				setIsModalVisible({ ...isModalVisible, isShowModal: false })
			);

			if (res && res.status === 200) {
				setListCountry(res.data);
				setIsModalVisible({ ...isModalVisible, isShowModal: false });
			}
		};

		fetchListCountry();
		fetchListSupplier();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const config = {
		title: 'Success Message!',
		content: <p>Success create supplier!</p>,
		onOk: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			window.location.reload();
		},
		onCancel: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
		},
	};

	const updateSupllier = {
		title: 'Success Message!',
		content: <p>Success update supplier!</p>,
		onOk: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			window.location.reload();
		},
		onCancel: () => {
			form.resetFields();
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
		},
	};

	const onFinish = async (values) => {
		setIsLoading(true);
		const data = {
			city: values.city,
			contact_number: values.contact_number,
			contact_number_alternate: values.contact_number_alternate,
			contact_person: values.contact_person,
			country_code: values.country_code,
			email: values.email,
			name: values.name,
			postcode: values.postcode,
			prefix: values.prefix,
			registration_number: values.registration_number,
			state: values.state,
			street: values.street,
			website: values.website,
			guid: '',
		};
		const res = await postNewSupplier(data).catch((err) => {
			window.alert(err?.response?.data?.message);
			setIsLoading(false);
		});

		if (res && res.status === 200) {
			modal.confirm(config);
			setIsLoading(false);
		}
	};

	const onEditSupplier = async (values) => {
		setIsLoading(true);
		const data = {
			city: values.city,
			contact_number: values.contact_number,
			contact_number_alternate: values.contact_number_alternate,
			contact_person: values.contact_person,
			country_code: values.country_code,
			email: values.email,
			name: values.name,
			postcode: values.postcode,
			prefix: values.prefix,
			registration_number: values.registration_number,
			state: values.state,
			street: values.street,
			website: values.website,
		};
		const res = await updateSupplier(data, values.guid).catch((err) => {
			window.alert(err?.response?.message);
			setIsLoading(false);
		});

		if (res && res.status === 200) {
			modal.confirm(updateSupllier);
			setIsLoading(false);
		}
	};

	const onDeleteSupplier = async (values) => {
		const res = await deleteSupplier(values).catch((err) => {
			window.alert(err?.response?.message);
		});

		if (res && res.status === 200) {
			window.alert('Success delete supplier');
			setIsModalActionVisible({
				...isModalActionVisible,
				isShowModalAction: false,
			});
			window.location.reload();
		}
	};

	const columnsTable = [
		{
			title: 'Supplier Name',
			dataIndex: 'name',
			key: 'name',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Contact Person',
			dataIndex: 'contact_person',
			key: 'contact_person',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Contact Number',
			dataIndex: 'contact_number',
			key: 'contact_number',
			render: (text) => <p>{text}</p>,
		},
		{
			title: 'Action',
			key: 'action',
			render: (text, record) => {
				return (
					<Space size='middle'>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'VIEW',
								})
							}
							type='primary'
							icon={<EyeOutlined />}>
							View
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'EDIT',
								})
							}
							type='default'
							icon={<EyeOutlined />}>
							Edit
						</Button>
						<Button
							onClick={() =>
								setIsModalActionVisible({
									...isModalActionVisible,
									isShowModalAction: true,
									dataRow: record,
									typeAction: 'DELETE',
								})
							}
							type='danger'
							icon={<EyeOutlined />}>
							Delete
						</Button>
					</Space>
				);
			},
		},
	];

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<div
				className='site-layout-background'
				style={{ padding: 24, textAlign: 'center' }}>
				<div
					style={{
						marginBottom: 12,
						justifyContent: 'flex-end',
						display: 'flex',
					}}>
					<Button
						onClick={() =>
							setIsModalActionVisible({
								...isModalActionVisible,
								typeAction: 'ADD',
								isShowModalAction: true,
							})
						}
						style={{ backgroundColor: '#389e0d', border: '1px solid #389e0d' }}
						type='primary'
						icon={<FileAddOutlined />}>
						Add Supplier
					</Button>
				</div>
				<Table columns={columnsTable} dataSource={listSupplier} />
			</div>
			<Modal
				title='Message'
				forceRender={false}
				visible={isShowModal}
				onOk={() =>
					setIsModalVisible({ ...isModalVisible, isShowModal: false })
				}
				onCancel={() =>
					setIsModalVisible({ ...isModalVisible, isShowModal: false })
				}>
				<div style={{ justifyContent: 'center', display: 'flex' }}>
					<Spin indicator={antIcon} />
				</div>
				<p style={{ textAlign: 'center', marginTop: 14 }}>{message}</p>
			</Modal>

			<Modal
				title={typeAction}
				forceRender={false}
				visible={isShowModalAction}
				width={typeAction === 'ADD' || typeAction === 'EDIT' ? 1000 : undefined}
				afterClose={() => form.resetFields()}
				onOk={() => {
					if (typeAction === 'DELETE') {
						onDeleteSupplier(dataRow.guid);
					} else {
						form.resetFields();
						setIsModalActionVisible({
							...isModalActionVisible,
							isShowModalAction: false,
						});
					}
				}}
				onCancel={() => {
					form.resetFields();
					setIsModalActionVisible({
						...isModalActionVisible,
						isShowModalAction: false,
					});
				}}>
				{typeAction === 'VIEW' ? (
					dataRow &&
					Object.keys(dataRow).map((data, index) => {
						if (
							data === 'key' ||
							data === 'guid' ||
							data === 'updated_at' ||
							data === 'created_at'
						) {
							return null;
						}
						return (
							<p key={index} style={{ fontWeight: 'bold', color: '#595959' }}>
								{data.replace('_', ' ')} :{' '}
								<span style={{ fontWeight: 'normal' }}>{dataRow[data]}</span>
							</p>
						);
					})
				) : typeAction === 'EDIT' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='addSupplier'
						onFinish={onEditSupplier}
						fields={[
							{
								name: ['name'],
								value: dataRow.name,
							},
							{
								name: ['country_code'],
								value: dataRow.country_code,
							},
							{
								name: ['state'],
								value: dataRow.state,
							},
							{
								name: ['city'],
								value: dataRow.city,
							},
							{
								name: ['street'],
								value: dataRow.street,
							},
							{
								name: ['postcode'],
								value: dataRow.postcode,
							},
							{
								name: ['contact_person'],
								value: dataRow.contact_person,
							},
							{
								name: ['contact_number'],
								value: dataRow.contact_number,
							},
							{
								name: ['contact_number'],
								value: dataRow.contact_number,
							},
							{
								name: ['contact_number_alternate'],
								value: dataRow.contact_number_alternate,
							},
							{
								name: ['email'],
								value: dataRow.email,
							},
							{
								name: ['registration_number'],
								value: dataRow.registration_number,
							},
							{
								name: ['guid'],
								value: dataRow.guid,
							},
						]}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='name'
									label='Supplier Name'
									rules={[
										{
											required: true,
											message: 'Please input your Supplier Name!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='country_code'
									label='Country'
									rules={[
										{
											required: true,
											message: 'Please input your Country!',
										},
									]}
									hasFeedback>
									<Select
										loading={isShowModal}
										placeholder='select your department'>
										{listCountry &&
											listCountry.map((data, index) => {
												const { name, alpha2Code } = data;
												return (
													<Option key={index} value={alpha2Code}>
														{name}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='state'
									label='State'
									rules={[
										{
											required: true,
											message: 'Please input your Supplier state!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='city'
									label='City'
									rules={[
										{
											required: true,
											message: 'Please input your city!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='street'
									label='Street/Building'
									rules={[
										{
											required: true,
											message: 'Please input your Supplier street!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='postcode'
									label='Postal Code/ZIP'
									rules={[
										{
											required: true,
											message: 'Please input your Postal Code/ZIP!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='contact_person'
									label='Contact Person'
									rules={[
										{
											required: true,
											message: 'Please input your Supplier Contact Person!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='contact_number'
									label='Primary Contact Number'
									rules={[
										{
											required: true,
											message:
												'Please input your Postal Primary Contact Number!',
										},
									]}
									hasFeedback>
									<Input
										addonBefore={prefixSelector}
										style={{
											width: '100%',
										}}
									/>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='contact_number_alternate'
									label='Alt. Contact Number'
									rules={[
										{
											required: true,
											message:
												'Please input your Supplier Alternate Contact Number!',
										},
									]}
									hasFeedback>
									<Input
										addonBefore={prefixSelector}
										style={{
											width: '100%',
										}}
									/>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='email'
									label='Email Address'
									rules={[
										{
											required: true,
											message: 'Please input your Postal Email Address!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='website'
									label='Website'
									rules={[
										{
											required: true,
											message: 'Please input your Supplier Website!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='registration_number'
									label='Company Reg. ID'
									rules={[
										{
											required: true,
											message:
												'Please input your Postal Company Registration ID!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<Form.Item hidden name='guid' label='guid'>
							<Input />
						</Form.Item>
						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Save
								</Button>
							</Form.Item>
							{contextHolder}
						</div>
					</Form>
				) : typeAction === 'ADD' ? (
					<Form
						{...formItemLayout}
						form={form}
						name='addSupplier'
						onFinish={onFinish}
						scrollToFirstError>
						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='name'
									label='Supplier Name'
									rules={[
										{
											required: true,
											message: 'Please input your Supplier Name!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='country_code'
									label='Country'
									rules={[
										{
											required: true,
											message: 'Please input your Country!',
										},
									]}
									hasFeedback>
									<Select
										loading={isShowModal}
										placeholder='select your department'>
										{listCountry &&
											listCountry.map((data, index) => {
												const { name, alpha2Code } = data;
												return (
													<Option key={index} value={alpha2Code}>
														{name}
													</Option>
												);
											})}
									</Select>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='state'
									label='State'
									rules={[
										{
											required: true,
											message: 'Please input your Supplier state!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='city'
									label='City'
									rules={[
										{
											required: true,
											message: 'Please input your city!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='street'
									label='Street/Building'
									rules={[
										{
											required: true,
											message: 'Please input your Supplier street!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='postcode'
									label='Postal Code/ZIP'
									rules={[
										{
											required: true,
											message: 'Please input your Postal Code/ZIP!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='contact_person'
									label='Contact Person'
									rules={[
										{
											required: true,
											message: 'Please input your Supplier Contact Person!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='contact_number'
									label='Primary Contact Number'
									rules={[
										{
											required: true,
											message:
												'Please input your Postal Primary Contact Number!',
										},
									]}
									hasFeedback>
									<Input
										addonBefore={prefixSelector}
										style={{
											width: '100%',
										}}
									/>
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='contact_number_alternate'
									label='Alt. Contact Number'
									rules={[
										{
											required: true,
											message:
												'Please input your Supplier Alternate Contact Number!',
										},
									]}
									hasFeedback>
									<Input
										addonBefore={prefixSelector}
										style={{
											width: '100%',
										}}
									/>
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='email'
									label='Email Address'
									rules={[
										{
											required: true,
											message: 'Please input your Postal Email Address!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<Row>
							<Col lg={12} md={24}>
								<Form.Item
									name='website'
									label='Website'
									rules={[
										{
											required: true,
											message: 'Please input your Supplier Website!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
							<Col lg={12} md={24}>
								<Form.Item
									name='registration_number'
									label='Company Reg. ID'
									rules={[
										{
											required: true,
											message:
												'Please input your Postal Company Registration ID!',
										},
									]}
									hasFeedback>
									<Input />
								</Form.Item>
							</Col>
						</Row>

						<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
							<Form.Item>
								<Button loading={isLoading} type='primary' htmlType='submit'>
									Create Supplier
								</Button>
							</Form.Item>
							{contextHolder}
						</div>
					</Form>
				) : (
					<p>Apakah anda ingin menghapus data ini ?</p>
				)}
			</Modal>
		</Content>
	);
};

export default Supplier;
