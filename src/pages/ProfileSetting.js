import {
	Button,
	Col,
	Form,
	Input,
	Layout,
	Row,
	Select,
	Avatar,
	Tag,
	Upload,
	Modal,
	Spin,
} from 'antd';
import React, { useEffect, useState } from 'react';
import {
	UserOutlined,
	UploadOutlined,
	LoadingOutlined,
} from '@ant-design/icons';
import { getMyProfile } from '../services/retrieveData';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
const { Content } = Layout;
const { Option } = Select;

const formItemLayout = {
	labelCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 8,
		},
	},
	wrapperCol: {
		xs: {
			span: 24,
		},
		sm: {
			span: 16,
		},
	},
};

const ProfileSetting = (props) => {
	const [form] = Form.useForm();
	const [isModalVisible, setIsModalVisible] = useState({
		isShowModal: false,
		message: '',
	});
	const [state, setState] = useState();
	const { isShowModal, message } = isModalVisible;

	const onFinish = (values) => {};

	useEffect(() => {
		const fetchMyProfile = async () => {
			setIsModalVisible({
				message: 'Harap bersabar halaman sedang kami muat',
				isShowModal: true,
			});
			const res = await getMyProfile().catch(() =>
				setIsModalVisible({ ...isModalVisible, isShowModal: false })
			);

			if (res && res.status === 200) {
				setState(res.data.data);
				setIsModalVisible({ ...isModalVisible, isShowModal: false });
			}
		};
		fetchMyProfile();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const prefixSelector = (
		<Form.Item name='prefix' noStyle>
			<Select
				style={{
					width: 70,
				}}>
				<Option value='62'>+62</Option>
				<Option value='87'>+87</Option>
			</Select>
		</Form.Item>
	);

	const normFile = (e) => {
		if (Array.isArray(e)) {
			return e;
		}
		return e && e.fileList;
	};

	return (
		<Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
			<div
				className='site-layout-background'
				style={{ padding: 24, textAlign: 'center' }}>
				<Row style={{ marginTop: 16 }} gutter={[24, 24]}>
					<Col xs={{ span: 24 }} lg={{ span: 24 }}>
						<div
							style={{
								borderRadius: 10,
								backgroundColor: 'white',
								border: '2px solid #d9d9d9',
								padding: 18,
							}}>
							<Row style={{ marginLeft: 120, marginBottom: 24 }}>
								<Col>
									<Avatar shape='square' size={64} icon={<UserOutlined />} />
								</Col>
								<Col>
									<div style={{ paddingLeft: 12, textAlign: 'left' }}>
										<p
											style={{
												color: '#595959',
												margin: 0,
												fontWeight: 'bold',
											}}>
											{state?.roles && state?.roles[0].label}
										</p>
										<p
											style={{
												color: '#595959',
												margin: 0,
											}}>
											{state?.email}
										</p>
										<Tag color='#2db7f5'>
											{state?.account_type + ' Account'}
										</Tag>
									</div>
								</Col>
							</Row>

							<Form
								{...formItemLayout}
								form={form}
								name='register'
								onFinish={onFinish}
								initialValues={{
									residence: ['zhejiang', 'hangzhou', 'xihu'],
									prefix: '62',
								}}
								fields={[
									{
										name: ['first_name'],
										value: state?.first_name,
									},
									{
										name: ['last_name'],
										value: state?.last_name,
									},
									{
										name: ['company'],
										value: state?.company?.name,
									},
									{
										name: ['department'],
										value: state?.company_department || 'N/A',
									},
									{
										name: ['job_title'],
										value: state?.job_title || 'N/A',
									},
									{
										name: ['employee_number'],
										value: state?.employee_number || 'N/A',
									},
									{
										name: ['phone_number'],
										value: state?.phone_number || 'N/A',
									},
								]}
								scrollToFirstError>
								<Row>
									<Col lg={12} md={24}>
										<Form.Item
											name='first_name'
											label='First Name'
											rules={[
												{
													required: true,
													message: 'Please input your First Name!',
												},
											]}
											hasFeedback>
											<Input disabled />
										</Form.Item>
									</Col>
									<Col lg={12} md={24}>
										<Form.Item
											name='last_name'
											label='Last Name'
											rules={[
												{
													required: true,
													message: 'Please input your Last Name!',
												},
											]}
											hasFeedback>
											<Input disabled />
										</Form.Item>
									</Col>
								</Row>

								<Row>
									<Col lg={12} md={24}>
										<Form.Item
											name='company'
											label='Company'
											rules={[
												{
													required: true,
													message: 'Please select company!',
												},
											]}
											hasFeedback>
											<Select disabled placeholder='select your company'>
												<Option value='male'>PT Wahyu</Option>
											</Select>
										</Form.Item>
									</Col>
									<Col lg={12} md={24}>
										<Form.Item
											name='department'
											label='Department'
											rules={[
												{
													required: true,
													message: 'Please select department!',
												},
											]}
											hasFeedback>
											<Select disabled placeholder='select your department'>
												<Option value='male'>IT Department</Option>
											</Select>
										</Form.Item>
									</Col>
								</Row>

								<Row>
									<Col lg={12} md={24}>
										<Form.Item
											name='job_title'
											label='Job Title'
											rules={[
												{
													required: true,
													message: 'Please input your Job Title',
												},
											]}
											hasFeedback>
											<Input disabled />
										</Form.Item>
									</Col>
									<Col lg={12} md={24}>
										<Form.Item
											name='employee_number'
											label='Empolyee ID'
											rules={[
												{
													required: true,
													message: 'Please input your Last Empolyee ID!',
												},
											]}
											hasFeedback>
											<Input disabled />
										</Form.Item>
									</Col>
								</Row>

								<Row>
									<Col lg={12} md={24}>
										<Form.Item
											name='phone_number'
											label='Phone Number'
											rules={[
												{
													required: true,
													message: 'Please input your phone number!',
												},
											]}
											hasFeedback>
											<Input
												disabled
												addonBefore={prefixSelector}
												style={{
													width: '100%',
												}}
											/>
										</Form.Item>
									</Col>
									<Col lg={12} md={24}>
										<Form.Item
											name='upload'
											label='Upload'
											valuePropName='fileList'
											getValueFromEvent={normFile}
											extra={'Disabled for now'}>
											<Upload
												disabled
												name='logo'
												action='/upload.do'
												listType='picture'>
												<Button icon={<UploadOutlined />}>
													Click to upload
												</Button>
											</Upload>
										</Form.Item>
									</Col>
								</Row>

								<div style={{ justifyContent: 'flex-end', display: 'flex' }}>
									<Form.Item>
										<Button disabled type='primary' htmlType='submit'>
											Save
										</Button>
									</Form.Item>
								</div>
							</Form>
						</div>
					</Col>
				</Row>
			</div>
			<Modal
				title='Message'
				forceRender={false}
				visible={isShowModal}
				onOk={() =>
					setIsModalVisible({ ...isModalVisible, isShowModal: false })
				}
				onCancel={() =>
					setIsModalVisible({ ...isModalVisible, isShowModal: false })
				}>
				<div style={{ justifyContent: 'center', display: 'flex' }}>
					<Spin indicator={antIcon} />
				</div>
				<p style={{ textAlign: 'center', marginTop: 14 }}>{message}</p>
			</Modal>
		</Content>
	);
};

export default ProfileSetting;
