import { Button, Col, Form, Input, Layout, Row } from 'antd';
import React, { useEffect, useState } from 'react';
import { auth } from '../services/auth';
import { postLogin } from '../services/retrieveData';

const { Content } = Layout;

const Login = (props) => {
	const [isLoading, setIsLoading] = useState(false);
	useEffect(() => {
		props.history.push(auth() ? '/dashboard' : '/');
	}, [props.history]);

	const onFinish = async (values) => {
		setIsLoading(true);
		const res = await postLogin(values).catch((err) => {
			window.alert('Failed login');
			setIsLoading(false);
		});

		if (res && res.status === 200) {
			localStorage.setItem('dataUser', JSON.stringify(res.data));
			props.history.push('/dashboard');
			setIsLoading(false);
		}
	};

	const onFinishFailed = (errorInfo) => {};

	return (
		<Layout style={{ backgroundColor: 'white' }}>
			<Row style={{ justifyContent: 'center' }}>
				<Col style={{ maxWidth: 450 }}>
					<Content>
						<div
							style={{
								border: '1px solid grey',
								borderRadius: 10,
								paddingRight: 12,
								paddingLeft: 12,
								paddingTop: 12,
								marginTop: 24,
							}}>
							<div>
								<Form
									name='basic'
									labelCol={{
										span: 8,
									}}
									wrapperCol={{
										span: 16,
									}}
									initialValues={{
										remember: true,
									}}
									onFinish={onFinish}
									onFinishFailed={onFinishFailed}>
									<Form.Item
										label='Email'
										name='email'
										rules={[
											{
												required: true,
												message: 'Please input your email!',
											},
										]}>
										<Input />
									</Form.Item>

									<Form.Item
										label='Password'
										name='password'
										rules={[
											{
												required: true,
												message: 'Please input your password!',
											},
										]}>
										<Input.Password />
									</Form.Item>

									<Form.Item
										wrapperCol={{
											offset: 8,
											span: 16,
										}}>
										<Button
											loading={isLoading}
											type='primary'
											htmlType='submit'>
											Log In
										</Button>
									</Form.Item>
								</Form>
								<p style={{ textAlign: 'center' }}>
									web admin create by
									<span>
										<a
											href='https://www.linkedin.com/in/wahyu-fatur-rizky/'
											rel='noreferrer'
											target='_blank'>
											{' '}
											wahyu fatur rizki
										</a>
									</span>
								</p>
							</div>
						</div>
					</Content>
				</Col>
			</Row>
		</Layout>
	);
};

export default Login;
