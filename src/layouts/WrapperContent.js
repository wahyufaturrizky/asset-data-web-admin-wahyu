import { Layout, Menu, Typography } from 'antd';
import React from 'react';
import {
	DashboardOutlined,
	UserOutlined,
	DropboxOutlined,
	LoginOutlined,
} from '@ant-design/icons';
import '../styles/Dashboard.css';

const { Title } = Typography;
const { Header, Footer, Sider } = Layout;

const WrapperContent = (props) => {
	const listMenu = [
		{
			title: 'Dashboard',
			nameIcon: 'DashboardOutlined',
			onClick: () => props.history.push('/dashboard'),
		},
		{
			title: 'Profile Setting',
			nameIcon: 'UserOutlined',
			onClick: () => props.history.push('/profile-setting'),
		},
		{
			title: 'Supplier',
			nameIcon: 'DropboxOutlined',
			onClick: () => props.history.push('/supplier'),
		},
		{
			title: 'Log Out',
			nameIcon: 'LoginOutlined',
			onClick: () => {
				if (window.confirm('Apakah anda ingin keluar dari halaman web ini?')) {
					localStorage.removeItem('dataUser');
					props.history.push('/');
				} else {
					props.history.push('/dashboard');
				}
			},
		},
	];
	return (
		<Layout>
			<Sider
				style={{
					overflow: 'auto',
					height: '100vh',
					position: 'fixed',
					left: 0,
				}}>
				<div className='logo'>
					<a
						href='https://www.linkedin.com/in/wahyu-fatur-rizky/'
						rel='noreferrer'
						target='_blank'>
						<Title
							level={5}
							style={{
								color: 'white',
								fontWeight: 'bold',
								textAlign: 'center',
							}}>
							Web Admin Wahyu
						</Title>
					</a>
				</div>
				<Menu
					theme='dark'
					mode='inline'
					defaultSelectedKeys={
						props?.match?.url === '/dashboard'
							? '1'
							: props?.match?.url === '/profile-setting'
							? '2'
							: props?.match?.url === '/supplier'
							? '3'
							: null
					}>
					{listMenu.map((data, index) => {
						const { title, nameIcon, onClick } = data;
						return (
							<Menu.Item
								onClick={onClick}
								key={index + 1}
								icon={
									nameIcon === 'DashboardOutlined' ? (
										<DashboardOutlined />
									) : nameIcon === 'UserOutlined' ? (
										<UserOutlined />
									) : nameIcon === 'DropboxOutlined' ? (
										<DropboxOutlined />
									) : nameIcon === 'LoginOutlined' ? (
										<LoginOutlined />
									) : null
								}>
								{title}
							</Menu.Item>
						);
					})}
				</Menu>
			</Sider>
			<Layout className='site-layout' style={{ marginLeft: 200 }}>
				<Header className='site-layout-background' style={{ padding: 0 }} />
				{props.children}
				<Footer style={{ textAlign: 'center' }}>
					<p>
						Wahyu Fatur Rizki @2021 Created by{' '}
						<span>
							<a
								href='https://www.linkedin.com/in/wahyu-fatur-rizky/'
								rel='noreferrer'
								target='_blank'>
								Wahyu
							</a>
						</span>
					</p>
				</Footer>
			</Layout>
		</Layout>
	);
};

export default WrapperContent;
