import {
	Request,
	RequestCountry,
	RequestPublic,
	RequestSupplier,
} from './headerRequest';
/**
 * @author wahyu fatur rizki (+62 822 7458 6011)
 * @return { obj }
 */

export const postLogin = (data) => {
	const response = RequestPublic().post('/hash-login', data);
	return response;
};

export const getMyProfile = () => {
	const response = Request().get('/me');
	return response;
};

export const getListSupplier = () => {
	const response = RequestSupplier().get('/setting/supplier');
	return response;
};

export const updateSupplier = (data, guid) => {
	const response = RequestSupplier().put(`/setting/supplier/${guid}`, data);
	return response;
};

export const postNewSupplier = (data) => {
	const response = RequestSupplier().post('/setting/supplier', data);
	return response;
};

export const deleteSupplier = (guid) => {
	const response = RequestSupplier().delete(`/setting/supplier/${guid}`);
	return response;
};

export const getListCountry = () => {
	const response = RequestCountry().get();
	return response;
};
