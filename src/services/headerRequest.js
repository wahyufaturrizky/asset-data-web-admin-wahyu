import axios from 'axios';
import { auth } from './auth';
/**
 * @author wahyu fatur rizki | linkedin.com/in/wahyu-fatur-rizky/
 * @return { obj }
 * Custom Header axios,
 * create from
 * using this function should axios().get(values)
 */

export const RequestPublic = () => {
	return axios.create({
		baseURL: process.env.REACT_APP_URL_API,
	});
};

export const Request = () => {
	return axios.create({
		baseURL: process.env.REACT_APP_URL_API,
		headers: {
			Authorization: `Bearer ${auth()}`,
		},
	});
};

export const RequestSupplier = () => {
	return axios.create({
		baseURL: process.env.REACT_APP_URL_API_SUPPLIER,
		headers: {
			Authorization: `Bearer ${auth()}`,
		},
	});
};

export const RequestCountry = () => {
	return axios.create({
		baseURL: process.env.REACT_APP_URL_API_COUNTRY,
	});
};
